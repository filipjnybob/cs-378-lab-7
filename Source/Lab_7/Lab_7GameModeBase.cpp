// Copyright Epic Games, Inc. All Rights Reserved.


#include "Lab_7GameModeBase.h"
#include "Lab7Character.h"
#include "Lab7CharacterController.h"

ALab_7GameModeBase::ALab_7GameModeBase() {
	PlayerControllerClass = ALab7CharacterController::StaticClass();

	static ConstructorHelpers::FObjectFinder<UClass> PawnBPClass(TEXT("Class'/Game/Blueprints/Lab7CharacterBP.Lab7CharacterBP_C'"));

	if (PawnBPClass.Object) {
		UClass* PawnBP = (UClass*)PawnBPClass.Object;
		DefaultPawnClass = PawnBP;
	}
	else {
		DefaultPawnClass = ALab7Character::StaticClass();
	}
}