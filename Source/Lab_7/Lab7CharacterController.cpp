// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab7CharacterController.h"
#include "Lab7Character.h"

const FName ALab7CharacterController::MoveForwardBinding("Forward");
const FName ALab7CharacterController::StrafeBinding("Strafe");

void ALab7CharacterController::SetupInputComponent() {
	Super::SetupInputComponent();

	InputComponent->BindAxis(MoveForwardBinding, this, &ALab7CharacterController::Move);
	InputComponent->BindAxis(StrafeBinding, this, &ALab7CharacterController::Strafe);
}

void ALab7CharacterController::Move(float value)
{
	ALab7Character* character = Cast<ALab7Character>(this->GetCharacter());
	if (character) {
		character->Move(value);
	}
}

void ALab7CharacterController::Strafe(float value)
{
	ALab7Character* character = Cast<ALab7Character>(this->GetCharacter());
	if (character) {
		character->Strafe(value);
	}
}
