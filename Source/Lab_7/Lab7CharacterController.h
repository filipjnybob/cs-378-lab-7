// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Lab7CharacterController.generated.h"

/**
 * 
 */
UCLASS()
class LAB_7_API ALab7CharacterController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void SetupInputComponent() override;

protected:
	static const FName MoveForwardBinding;
	static const FName StrafeBinding;

	UFUNCTION()
		void Move(float value);

	UFUNCTION()
		void Strafe(float value);

};
